
fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(result => {
	console.log(result)
})



 fetch('https://jsonplaceholder.typicode.com/todos/2')
.then(res => res.json())
.then(result => {
	console.log(result)
	console.log ("")
})
 


 fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		title: 'New Post',
		status: 'Hello Again',
		DateCompleted:'',
		userId: 2
	})
 })
.then(res => res.json())
.then(data => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Hello Again',
        DateCompleted: '12/5/20',
        userId: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))



fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'Hello Again AGAAAAIN',
        userID: 1
    })
})
.then(response => response.json())
.then(data => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
.then(res=> res.json())
.then(data=>console.log(data))

